//Я так понял, что асинхронность происходит в Event loop самого браузера, а JS дает только команды в каком порядке выполнять код


const btn = document.querySelector('#ipBtn');

btn.addEventListener("click", async function () {
  const userIp = await fetch("https://api.ipify.org/?format=json");
  const ip = await userIp.json();

  const geoPosition = await fetch(`http://ip-api.com/json/${ip.ip}?lang=ru&fields=continent,country,region,city,district`);
  const locationInfo = await geoPosition.json();

  const infoList = document.createElement("div");
  infoList.setAttribute("id", "container");

  for (let [key, value] of Object.entries(locationInfo)) {
    infoList.innerHTML += `<p>${key} - ${value}</p>`;
  }

  if (!document.getElementById("container")) {
    btn.after(infoList);
  } else {
    document.body.replaceChild(infoList, document.getElementById("container"))
  }
});